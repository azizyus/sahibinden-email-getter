<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.04.2018
 * Time: 21:45
 */

namespace App\PropertyClasses;
use Curl\Curl;

class SahibindenPageLinkCreator
{



//https://www.sahibinden.com/emlak?viewType=Classic&pagingOffset=50&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1


    public $pageCount=0;
    public $curl;
    public $baseUrl ="https://www.sahibinden.com/emlak?viewType=Classic&pagingOffset=50&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1";
    public $basePageHtml;
    public $totalPageNumber=null;
    public $allLinks;
    public $pageSize=50;
    public function __construct()
    {


        $this->curl = new Curl();



        $this->basePageHtml = $this->curl->get($this->baseUrl);

        $this->allLinks=collect();

    }
    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function getTotalPageCount() : int
    {


        if($this->totalPageNumber==Null)
        {

            $pharse = \Pharse::str_get_dom($this->basePageHtml);


            $item = $pharse(".mbdef") [0];

            $totalPageNumber = $this->get_string_between($item->getInnerText(),"Toplam","sayfa");
            $totalPageNumber = str_replace(" ","",$totalPageNumber);
            $totalPageNumber = (int)$totalPageNumber;


            $this->totalPageNumber = $totalPageNumber;
        }

        return $this->totalPageNumber;




    }

    public function getOtherLinks()
    {


        if($this->allLinks->count()==0)
        {

            $baseUrl = $this->baseUrl;

            for($i=0;$i<$this->getTotalPageCount();$i++)
            {


                $nextOffset = $i*$this->pageSize;
                $nextUrl = str_replace("pagingOffset=50","pagingOffset=".$nextOffset,$baseUrl);

                $this->allLinks->push($nextUrl);


            }

        }


       return $this->allLinks;
    }

}