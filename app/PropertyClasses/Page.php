<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.04.2018
 * Time: 21:25
 */

namespace App\PropertyClasses;

use App\Property;
class Page
{




    public $pageHtmlCodes;
    public $properties;
    public function __construct($pageHtmlCodes)
    {

        $this->pageHtmlCodes = $pageHtmlCodes;
        $this->properties = collect();

    }

public function getProperties()
{
    return $this->properties;
}

    public function getResult()
    {

        return $this->pageHtmlCodes;

    }

    public function addProperty(String $link)
    {


        $property = new Property();
        $property->link = $link;

        $this->properties->push($property);


    }
}