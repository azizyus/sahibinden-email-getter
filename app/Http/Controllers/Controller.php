<?php

namespace App\Http\Controllers;

use App\PropertyClasses\PropertyBot;
use App\PropertyClasses\SahibindenPageLinkCreator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function test()
    {



        $linkCreator = new SahibindenPageLinkCreator();

        $allLinks  = $linkCreator->getOtherLinks();



        $propertyBot = new PropertyBot();


        foreach ($allLinks as $link)
        {


            $page = $propertyBot->addPage($link);
            $propertyBot->putPageLinks($page);

        }



        $propertyBot->fillPagesPropertyDetail();




    }
}
