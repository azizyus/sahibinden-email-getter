<?php

namespace App\Console\Commands;

use App\PropertyClasses\PropertyBot;
use App\PropertyClasses\SahibindenPageLinkCreator;
use Illuminate\Console\Command;

class GetPropertyLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dd:gl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);

        $linkCreator = new SahibindenPageLinkCreator();

        $allLinks  = $linkCreator->getOtherLinks();



        $propertyBot = new PropertyBot();


        foreach ($allLinks as $link)
        {


            $page = $propertyBot->addPage($link);
            $propertyBot->putPageLinks($page);
          //  break;

        }



        $propertyBot->fillPagesPropertyDetail();




      //  file_put_contents(public_path("result.json"),json_encode($propertyBot->pages->pluck("properties")));



    }
}
